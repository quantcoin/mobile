# QCoin React Native mobile wallet
## Building
### **Prerequesites** 
### *Install node*
### *Install react-native*
> npm install -g react-native-cli
### **Installation** 
> git clone https://gitlab.com/qcoin/graphene-mobile-ui.git

> npm install

### **Running** 
To run on Android launch simulator and then exec:

> react-native run-android

To run on iOS launch simulator and then exec:

> react-native run-ios

### **Architecture**