/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import './shim'

import React, { Component } from 'react';
import {
  Text,
  AppRegistry,
} from 'react-native';

import App from './app/app';

export default class QCoinWallet extends Component {
  render() {
    return (
      <App/>
    );
  }
}
AppRegistry.registerComponent('QCoinWallet', () => QCoinWallet);
