export default Strings = {
    createWallet: "Create wallet",
    createAccount: "Create account",

    enterPassword: "Enter password",
    confirmPassword: "Confirm password",
    enterAccountName: "Enter account name"
}