import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import LoginScreen from './components/LoginScreen/LoginScreen';

import { ChainStore } from "graphenejs-lib";
import { Apis } from "graphenejs-ws";
import AccountStore from "./stores/AccountStore";

export default class App extends Component {
    render() {
        return (
            <LoginScreen />
        )
    }

    componentDidMount() {
        try {
            Apis.instance("ws://graphene-witness.westeurope.cloudapp.azure.com/witness_api", true).init_promise.then((res) => {
                // console.log("connected to:", res[0].network);
                ChainStore.init().then(() => {
                    ChainStore.subscribe(updateState);
                });
            });

            let dynamicGlobal = null;
            function updateState(object) {
                dynamicGlobal = ChainStore.getObject("2.1.0");

                console.log("ChainStore object update\n", dynamicGlobal ? dynamicGlobal.toJS() : dynamicGlobal);
            }




            // ChainStore.init().then(() => {
            //         this.setState({synced: true});

            //         Promise.all([
            //             AccountStore.loadDbData(Apis.instance().chainId)
            //         ]).then(() => {
            //             AccountStore.tryToSetCurrentAccount();
            //             this.setState({loading: false, syncFail: false});
            //         }).catch(error => {
            //             console.log("[App.jsx] ----- ERROR ----->", error);
            //             this.setState({loading: false});
            //         });
            //     }).catch(error => {
            //         console.log("[App.jsx] ----- ChainStore.init error ----->", error);
            //         let syncFail = error.message === "ChainStore sync error, please check your system clock" ? true : false;
            //         this.setState({loading: false, syncFail});
            //     });
        } catch (e) {
            console.error("e:", e);
        }
    }
}
