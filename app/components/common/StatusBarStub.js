import React, { Component } from 'react';
import {
    View
} from 'react-native';

export default class StatusBarStub extends Component {
    render () {
        return (
            <View style={{height: 20}}/>
        )
    }
}