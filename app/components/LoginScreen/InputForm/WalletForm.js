import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView,
    StatusBar
} from 'react-native';

import OneLineInput from './OneLineInput';
import Local from '../../../localization/default';

export default class WalletForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            
        }
    }

    firstPass = "";
    secondPass = "";

    //ios secureTextEntry glitch - when one char and switching - expected behaviour
    render() {
        return (
            <View style={[this.props.style, {justifyContent: 'center'}]}>
                <OneLineInput onUpdatedCallback={(id, str) => this._onInputUpdated(id,str)} id={0} secureTextEntry={true} placeholder={Local.enterPassword}/>
                <OneLineInput onUpdatedCallback={(id, str) => this._onInputUpdated(id, str)} id={1} secureTextEntry={true} placeholder={Local.confirmPassword}/>
                {this.state.infoText}
            </View>
        )
    }
    _isPasswordValid(password) {
        return password.length >= 8;
    }

    _onInputUpdated(id, str) {
        if (id === 0)
            this.firstPass = str;
        else
            this.secondPass = str;

        var paswordsValid = true;
        if (!this._isPasswordValid(this.firstPass)
        || !this._isPasswordValid(this.secondPass)
        || this.firstPass !== this.secondPass) {
            paswordsValid = false;
        }

        this.props.onWalletFormStateChanged(paswordsValid, this.firstPass);
    }


}

WalletForm.propTypes = {
    onWalletFormStateChanged: React.PropTypes.func.isRequired
}