import React, { Component } from 'react';
import { View, TextInput } from 'react-native';

import AppStyle from '../../../style/AppStyle';

export default class OneLineTextInput extends Component {
    constructor(props) {
        super(props);
        this.state = { text: '', focused: false };
    }

    render() {
        return (
            <View style={style.container}>
                <TextInput
                    style={[style.textInput, this.props.style]}
                    onChangeText={(text) => this._onChangeText(text)}
                    value={this.state.text}
                    placeholder={this.props.placeholder}
                    placeholderTextColor={AppStyle.colors.subtitleText}
                    secureTextEntry={this.props.secureTextEntry}
                    ref="input"
                />

                <View style={style.underline} />
            </View>
        );
    }

    _focus() {
        this.refs["input"].focus();
    }

    _onChangeText(text) {
        this.setState({text: text});
        this.props.onUpdatedCallback(this.props.id, text)
        // this.props.on
    }
}

OneLineTextInput.propTypes = {
    id: React.PropTypes.number.isRequired,
    onUpdatedCallback: React.PropTypes.func.isRequired
}

const style = {
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 24
    },
    textInput: {
        height: 40,
        color: AppStyle.colors.whiteText,
        width: 280,
        fontSize: 20,
        alignSelf: 'center',
        marginLeft: 10
    },
    underline: {
        height: 1,
        width: 280,
        backgroundColor: AppStyle.colors.inputFormLine,
        marginTop: -2
    }
}