import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView,
    StatusBar,
} from 'react-native';

import AppStyle from '../../style/AppStyle';
import CircleSnail from 'react-native-progress/CircleSnail';

export default class SplashScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }

    render() {
        return (
            <View style={[this.props.style, styles.container]}>
            </View>
        )
    }
}

styles = {
    container: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: AppStyle.colors.splashScreen,
        opacity: AppStyle.opacity.splashScreenOpacity
    }
}