/*import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    TouchableHighlight,
} from 'react-native';

import {OneLineTextInput} from './OneLineInput';

const WALLET_MODE = Symbol('formModeWallet');
const ACCOUNT_MODE = Symbol('formModeAccount');

class SubmitButton extends Component {
    render () {
        return (
            <TouchableHighlight underlayColor='#00d'
                                        style={styles.fullWidthButton}
                                      onPress={this.props.onPress.bind(this)}>
                <Text style={styles.fullWidthButtonText}>{this.props.text}</Text>
            </TouchableHighlight>
        )
    }
}

export default class InputForm extends Component {
    constructor(props) {
        super(props);
        this.state = {mode: WALLET_MODE};
    }

    componentDidMount() {
        if (this.refs.firstPassword) {
            this.refs.firstPassword.focus();
        }
    }

    render() {
        return (
        <View>
            <OneLineTextInput style={styles.inputForm}
                    secureTextEntry={true}
                    placeholder={"Enter password"}
                    ref="firstPassword"/>
            <OneLineTextInput style={styles.inputForm}
                    secureTextEntry={true}
                    placeholder={"Repeat password"}
                    ref="secondPassword"/>

            <SubmitButton text={this.state.mode}//{this._buttonTextFromMode(this.state.mode)}
                       onPress={this._createButtonPressed.bind(this)}/>
        </View>
        )
    }

    validate(state) {
        var {password, confirm} = state
        confirm = confirm.trim()
        password = password.trim()

        var errors = Immutable.Map()
        // Don't report until typing begins
        if(password.length !== 0 && password.length < 8)
            errors = errors.set("password_length", "Password must be 8 characters or more")

        // Don't report it until the confirm is populated
        if( password !== "" && confirm !== "" && password !== confirm)
            errors = errors.set("password_match", "Passwords do not match")

        var valid = password.length >= 8 && password === confirm
        this.setState({errors, valid})
        this.props.onValid(valid ? password : null)
    }

    _createButtonPressed(e) {
        this.setState({mode: ACCOUNT_MODE});
    }

    _buttonTextFromMode(mode) {
        switch (mode) {
            case WALLET_MODE:
                return "Create wallet";
                break;
            case ACCOUNT_MODE:
                return "Create account";
                break;
            default:
                break;
        }
    }
}

const styles = StyleSheet.create({
    fullWidthButton: {
        // backgroundColor: 'blue',
        height:70,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        backgroundColor:'#68a0cf',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
    },

    inputForm: {
        borderRadius:10,
        borderWidth: 1,
        paddingLeft: 5,
        // borderColor: '#fff',
        margin: 10
    },

    fullWidthButtonText: {
        fontSize:24,
        color: 'white'
    }
});*/