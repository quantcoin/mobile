import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    StatusBar,
    TextInput,
} from 'react-native';


import AppStyle from '../../style/AppStyle';
import Local from '../../localization/default';

import StatusBarStub from '../common/StatusBarStub';
import AccountForm from './InputForm/AccountForm';
import WalletForm from './InputForm/WalletForm';
import SplashScreen from './SplashScreen';
import ActionButton from './ActionButton';

import AccountStore from '../../stores/AccountStore';
import WalletDb from '../../stores/WalletDb';
import WalletActions from '../../actions/WalletActions';

export default class LoginScreen extends React.Component {

    modes = {
        createWallet: Symbol("createWallet"),
        creatingWallet: Symbol("creatingWallet"),
        createAccount: Symbol("createAccount")
    }

    constructor(props) {

        super(props);

        this.state = {
            mode: this.modes.createWallet,
            buttonEnabled: false
        }
    }

    password = "";

    componentWillMount() {

    }

    render() {
        let my_accounts = AccountStore.getMyAccounts();
        WalletDb.getWallet();
        switch (this.state.mode) {
            case this.modes.createWallet:
                return this._renderCreateWallet();

            case this.modes.creatingWallet:
                return this._renderLoadingIndicator();

            case this.modes.createAccount:
                return this._renderCreateAccount();
        }
        return this._renderReal();
    }

    _renderCreateWallet() {
        
        return (
            <View style={styles.container}>
                <StatusBarStub />
                <View style={styles.titleContainer}>
                    <Text style={styles.titleText}>QCoin Wallet</Text>
                </View>
                <WalletForm style={styles.walletForm}
                    onWalletFormStateChanged={(state, password) => this._onWalletFormStateChanged(state, password)} />
                <ActionButton enabled={this.state.buttonEnabled} title={this._buttonTextFromMode(this.state.mode)} onPress={(e) => this._createWalletPressed(e)} />
            </View>
        )
    }

    _renderTest() {
        return (

            <TextInput style={{ width: 100, height: 30, borderColor: 'gray', borderWidth: 1, alignSelf: 'center' }} />

        )
    }

    componentDidMount() {

    }

    _buttonTextFromMode(mode) {
        if (mode === this.modes.createWallet || mode === this.modes.creatingWallet) {
            return Local.createWallet;
        }
        if (mode === this.modes.createAccount) {
            return Local.createAccount;
        }
    }

    // handlers

    _createWalletPressed(e) {
        this._createWalet(this.password).then(() => {
            console.log("created");
        })
    }

    _onWalletFormStateChanged(state, password) {
        this.setState({ buttonEnabled: state });

        this.password = password;
    }

    // actions

    _createWalet(password) {
        return WalletActions.setWallet(
            "default", //wallet name
            password
        ).then(() => {
            console.log("Congratulations, your wallet was successfully created.");
        }).catch(err => {
            console.log("CreateWallet failed:", err);
            // notify.addNotification({
            //     message: `Failed to create wallet: ${err}`,
            //     level: "error",
            //     autoDismiss: 10
            // })
        });
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: AppStyle.colors.mainBG,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1
    },
    walletForm: {
        flex: 3,
        marginBottom: 42
    },
    titleText: {
        fontSize: 36,
        color: AppStyle.colors.whiteText
    },
    titleContainer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    }
});