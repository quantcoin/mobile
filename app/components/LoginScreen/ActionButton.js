import React, { Component } from 'react';
import {
    TouchableHighlight,
    StyleSheet,
    Text
} from 'react-native';

import AppStyle from '../../style/AppStyle';

export default class ActionButton extends Component {

    constructor(props) {
        super(props);
        // this.activeButtonStyle = this.props.enabled ? styles.enabledButton : styles.disabledButton;
    }

    // activeButtonStyle;

    render() {
        var activeButtonStyle = this.props.enabled ? styles.enabledButton : styles.disabledButton;
        return (
            <TouchableHighlight style={[styles.common, activeButtonStyle]}
                disabled={!this.props.enabled}
                onPress={(e) => this.props.onPress(e)}>
                <Text style={styles.buttonText}>{this.props.title}</Text>
            </TouchableHighlight>
        )
    }
}

ActionButton.propTypes = {
    enabled: React.PropTypes.bool,
    title: React.PropTypes.string,
    onPress: React.PropTypes.func
}

ActionButton.defaultProps = {
    enabled: true,
    title: "",
}

styles = {
    common: {
        height: 70,
        width: 280,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 42,
        borderRadius: 4
    },
    enabledButton: {
        backgroundColor: AppStyle.colors.blueButton,
    },
    disabledButton: {
        backgroundColor: AppStyle.colors.disabledButton,
    },
    buttonText: {
        fontSize: 24,
        color: AppStyle.colors.whiteText
    },
}