import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    TouchableHighlight,
    Navigator,
} from 'react-native';


var AccountActions = require("../../actions/AccountActions");

import InputForm from './InputForm';
import StatusBar from '../common/StatusBar';
import CircleSnail from 'react-native-progress/CircleSnail';
import Test from '../test';

export default class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {isCreatingWallet: false}
    }

    render() {

        let body = [<InputForm key={"input"}/>];

        if (this.state.isCreatingWallet) {
            body.unshift(<CircleSnail key={"loading"} size={30} animating={true}
                                    style={{justifyContent: 'center',alignItems: 'center'}}/>);
        }

        return (
            <View style={styles.container}>
                <StatusBar/>
                <View style={styles.header}>
                    <Text style={styles.headerTitle}>
                        QCoin Wallet
                    </Text>
                </View>

                <View style={styles.body}>
                    {body}
                </View>
            </View>
        )
    }


    createAccount(name) {
        //let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        WalletUnlockActions.unlock().then(() => {
            this.setState({loading: true});
            AccountActions.createAccount(name, this.state.registrar_account, this.state.registrar_account, 0, refcode).then(() => {
                // User registering his own account
                if(this.state.registrar_account) {
                    this.setState({loading: false});
                    TransactionConfirmStore.listen(this.onFinishConfirm);
                } else { // Account registered by the faucet
                    console.log("account registed by faucet");
                    // this.props.history.pushState(null, `/wallet/backup/create?newAccount=true`);
                    this.setState({
                        step: 2
                    });
                    // this.props.history.pushState(null, `/account/${name}/overview`);

                }
            }).catch(error => {
                console.log("ERROR AccountActions.createAccount", error);
                let error_msg = error.base && error.base.length && error.base.length > 0 ? error.base[0] : "unknown error";
                if (error.remote_ip) error_msg = error.remote_ip[0];
                notify.addNotification({
                    message: `Failed to create account: ${name} - ${error_msg}`,
                    level: "error",
                    autoDismiss: 10
                });
                this.setState({loading: false});
            });
        });
    }

    createWallet(password) {
        return WalletActions.setWallet(
            "default", //wallet name
            password
        ).then(()=> {
            console.log("Congratulations, your wallet was successfully created.");
        }).catch(err => {
            console.log("CreateWallet failed:", err);
            notify.addNotification({
                message: `Failed to create wallet: ${err}`,
                level: "error",
                autoDismiss: 10
            })
        });
    }

    onSubmit(e) {
        //e.preventDefault();
        //if (!this.isValid()) return;
        let account_name = this.accountNameInput.getValue();
        if (WalletDb.getWallet()) {
            this.createAccount(account_name);
        } else {
            let password = this.refs.password.value();
            this.createWallet(password).then(() => this.createAccount(account_name));
        }
    }


    buttonPressed() {
        //AccountActions.
    }
}

const styles = StyleSheet.create({
    container: {
        
        ...StyleSheet.absoluteFillObject,
    },
        fullWidthButton: {
        backgroundColor: 'blue',
        height:70,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
  },
  fullWidthButtonText: {
    fontSize:24,
    color: 'white'
  },
    gradient: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    header: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 48,
        textAlign: 'center',
        justifyContent: 'center'
    },
    body: {
        flex: 3,
        justifyContent: 'center'
    },
});