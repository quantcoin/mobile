export default AppStyle = {
    colors: {
        mainBG : "#2a2a2a",
        subtitleText: "#a1a1a1",
        blueButton: "#2096f3",
        inputFormLine: "#979797",
        whiteText: "#fff",
        splashScreen: "#4d4d4d",
        selectedButton: "#666",
        disabledButton: "#333",
    },
    opacity: {
        splashScreenOpacity: 0.36
    }
}